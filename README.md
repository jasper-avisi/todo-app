# Todo App

## Local Development

### Install and configure asdf

asdf is used to configure the correct versions of needed tooling in your local environment. Download
asdf, specific instructions for your OS are available in the documentation:
http://asdf-vm.com/guide/getting-started.html#_2-download-asdf

When running OSX with brew:
`brew install asdf`

asdf needs to be configured in you bash profile (see documentation). When installed with brew, put
the following in your `.profile` or other shell initialization script:

```
. $(brew --prefix asdf)/libexec/asdf.sh
. $(brew --prefix asdf)/etc/bash_completion.d/asdf.bash
```

asdf plugins need to be installed for the used tools. Run the following commands in the project root:

```
asdf plugin add nodejs
asdf plugin add java
asdf install
```

## Application Specs

### Frontend

**Docker image url**
`registry.gitlab.com/avisi/avisi_academy/todo-app/frontend:latest`

**Exposed Ports**

* **8080** Angular app

Note: The back-end api's are called with the relative url `/api/` and should be routed to the backend application. See the `dev-proxy/nginx.conf` for an example.

**Container environment**
User: nginx (UID: 101)

### Backend

**Docker image url**
`registry.gitlab.com/avisi/avisi_academy/todo-app/backend:latest`

**Exposed Ports**

* **8080** Api endpoint on `/api/todos`
* **8081** Health check endpoint

**HealthChecks**
Readiness probe: `http://localhost:8081/actuator/health/readiness`
Liveness probe:  `http://localhost:8081/actuator/health/liveness`

**Container environment**
User: spring-boot (UID: 1000)

**Environment Variables**

| Variable                     | Description                       | Example                                |
|:-----------------------------|-----------------------------------|----------------------------------------|
| `SPRING_DATASOURCE_URL`      | JDBC url to the postgres database | `jdbc:postgresql://postgres:5432/todo` |
| `SPRING_DATASOURCE_USERNAME` | Database username                 |                                        |
| `SPRING_DATASOURCE_PASSWORD` | Database password                 |                                        |

