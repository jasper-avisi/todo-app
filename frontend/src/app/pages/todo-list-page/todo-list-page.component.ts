import { Component, OnInit } from "@angular/core";
import { TodoService } from "../../services/todo.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-todo-list-page",
  templateUrl: "./todo-list-page.component.html",
})
export class TodoListPageComponent {

  public todos$ = this.todoService.getTodoList();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private todoService: TodoService
  ) {}

  addTodo() {
    this.router.navigate(["add"], { relativeTo: this.route });
  }
}
