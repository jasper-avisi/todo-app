import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { TodoService } from "../../services/todo.service";

@Component({
  selector: "app-add-todo-page",
  templateUrl: "./add-todo-page.component.html",
})
export class AddTodoPageComponent implements OnInit {
  public form = new FormGroup({
    title: new FormControl("", {
      nonNullable: true,
      validators: [Validators.required],
    }),
    body: new FormControl<string | null>(null),
  });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private todoService: TodoService
  ) {}

  ngOnInit(): void {}

  submitForm() {
    if (this.form.valid) {
      this.todoService
        .addTodo({
          title: this.form.controls.title.value,
          body: this.form.controls.body.value || undefined,
        })
        .subscribe(() => {
          this.router.navigate(["../"], { relativeTo: this.route });
        });
    }
  }

  cancel() {
    this.router.navigate(["../"], { relativeTo: this.route });
  }
}
