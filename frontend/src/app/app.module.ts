import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TodoListPageComponent } from "./pages/todo-list-page/todo-list-page.component";
import { HttpClientModule } from "@angular/common/http";
import { AddTodoPageComponent } from "./pages/add-todo-page/add-todo-page.component";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [AppComponent, TodoListPageComponent, AddTodoPageComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
