import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TodoListPageComponent } from "./pages/todo-list-page/todo-list-page.component";
import { AddTodoPageComponent } from "./pages/add-todo-page/add-todo-page.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "todos",
    pathMatch: "full",
  },
  {
    path: "todos",
    component: TodoListPageComponent,
  },
  {
    path: "todos/add",
    component: AddTodoPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
