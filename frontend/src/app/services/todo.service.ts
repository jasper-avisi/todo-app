import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Todo, TodoRequest } from "../models/todo-model";

@Injectable({
  providedIn: "root",
})
export class TodoService {
  todoApiUrl = "/api/todos";

  constructor(private http: HttpClient) {}

  getTodoList(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.todoApiUrl);
  }

  addTodo(request: TodoRequest): Observable<Todo> {
    return this.http.post<Todo>(this.todoApiUrl, request);
  }
}
