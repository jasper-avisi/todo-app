export interface Todo {
  id: string;
  title: string;
  body: string;
  created_at: string;
  updated_at: string;
}

export interface TodoRequest {
  title: string;
  body?: string;
}
