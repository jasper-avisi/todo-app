.PHONY: build


build: build-backend build-frontend

install:
	cd frontend && npm install

build-backend:
	cd backend && ./gradlew build

build-frontend:
	cd frontend && npm run build

docker: build
	cd backend && docker build . -t registry.gitlab.com/avisi/avisi_academy/todo-app/backend:latest
	cd frontend && docker build . -t registry.gitlab.com/avisi/avisi_academy/todo-app/frontend:latest

up:
	docker-compose up -d --build

stop:
	docker-compose stop

down:
	docker-compose down

postgres:
	docker-compose up -d postgres
