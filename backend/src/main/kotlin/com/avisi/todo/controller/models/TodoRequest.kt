package com.avisi.todo.controller.models

data class TodoRequest(
  val title: String,
  val body: String,
)
