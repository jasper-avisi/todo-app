package com.avisi.todo.controller.models

import java.time.OffsetDateTime
import java.util.*

data class Todo(
  val id: UUID,
  val title: String,
  val body: String,
  val createdAt: OffsetDateTime,
  val updatedAt: OffsetDateTime
)
