package com.avisi.todo.controller.mapper

import com.avisi.todo.controller.models.Todo
import com.avisi.todo.controller.models.TodoRequest
import com.avisi.todo.repository.models.TodoEntity
import com.avisi.todo.utils.toOffsetDateTime


fun TodoEntity.toRest(): Todo = Todo(
  id = this.id!!,
  title = this.title,
  body = this.body,
  createdAt = this.createdAt!!.toOffsetDateTime(),
  updatedAt = this.updatedAt!!.toOffsetDateTime()
)

fun TodoRequest.toEntity() = TodoEntity(
  id = null,
  title = this.title,
  body = this.body
)
