package com.avisi.todo.controller

import com.avisi.todo.controller.mapper.toEntity
import com.avisi.todo.controller.mapper.toRest
import com.avisi.todo.controller.models.Todo
import com.avisi.todo.controller.models.TodoRequest
import com.avisi.todo.repository.TodoRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/todos")
class TodoController(
  private val todoRepository: TodoRepository
) {

  @GetMapping
  fun getTodos(): List<Todo> = todoRepository.findByOrderByCreatedAtDesc().map { it.toRest() }

  @PostMapping
  fun addTodo(@RequestBody todo: TodoRequest) = todoRepository.save(todo.toEntity()).toRest()

}
