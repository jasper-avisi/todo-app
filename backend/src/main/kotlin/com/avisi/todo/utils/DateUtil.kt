package com.avisi.todo.utils

import java.time.Instant
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZoneOffset

/**
 * Use this method to convert from an Instant, used in de audit fields of the Repository, to an OffsetDateTime that is
 * used in the api controller. This function also makes sure the datetime is returned as UTC datetime.
 */
fun Instant.toOffsetDateTime(): OffsetDateTime = this.atOffset(ZoneOffset.UTC)


fun LocalDate.toInstant(): Instant = this.atStartOfDay().toInstant(ZoneOffset.UTC)
