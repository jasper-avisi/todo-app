package com.avisi.todo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jdbc.repository.config.EnableJdbcAuditing

@SpringBootApplication
@EnableJdbcAuditing
class TodoApplication

fun main(args: Array<String>) {
	runApplication<TodoApplication>(*args)
}
