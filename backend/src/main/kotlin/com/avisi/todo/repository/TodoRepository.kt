package com.avisi.todo.repository

import com.avisi.todo.repository.models.TodoEntity
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface TodoRepository : CrudRepository<TodoEntity, UUID> {

  fun findByOrderByCreatedAtDesc(): List<TodoEntity>

}
