package com.avisi.todo.repository.models

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant
import java.util.*

@Table("todo")
data class TodoEntity(
  @Id
  val id: UUID?,
  val title: String,
  val body: String,
  @CreatedDate
  val createdAt: Instant? = null,
  @LastModifiedDate
  val updatedAt: Instant? = null
)
