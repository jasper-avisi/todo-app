CREATE EXTENSION IF NOT EXISTS "pgcrypto";

/*
    Tables
*/
CREATE TABLE "todo"
(
  "id"         UUID PRIMARY KEY     DEFAULT gen_random_uuid(),
  "title"      text,
  "body"       text,
  "created_at" timestamptz NOT NULL default now(),
  "updated_at" timestamptz
);

